package activity.blattusz.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class AddPont extends Activity {

    boolean pontkiadva = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pont);

        ListView listView;
        ArrayList<String> listItems;
        ArrayAdapter<String> adapter;

        ListView listView2;
        ArrayList<Integer> listItems2;
        ArrayAdapter<Integer> adapter2;


        listView = (ListView) findViewById(R.id.players);
        listItems = new ArrayList<String>();
        listItems.addAll(MainActivity.Players);
        adapter = new ArrayAdapter<String>(this, R.layout.playerlist, listItems);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {


               // MainActivity.PlayersPontsz.add((int) id,MainActivity.jutalompont);
                if(pontkiadva == false)
                {
                    MainActivity.PlayersPontsz.set((int) id, MainActivity.PlayersPontsz.get((int) id) + MainActivity.jutalompont);
                    RefreshList();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Ebben a körben nem adhatsz másnak pontot", Toast.LENGTH_LONG).show();
                }



            }

        });



        listView2 = (ListView) findViewById(R.id.pontszamok);
        listItems2 = new ArrayList<>();
        listItems2.addAll(MainActivity.PlayersPontsz);
        adapter2 = new ArrayAdapter<Integer>(getApplicationContext(), R.layout.pontszamok, listItems2);
        listView2.setAdapter(adapter2);
        adapter2.notifyDataSetChanged();



        Button backbtn = (Button) findViewById(R.id.back);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                End();
            }
        });
    }

public void End()
{
    this.finish();
}

    public void RefreshList()
    {
            pontkiadva = true;
            ListView listView;
            ArrayList<Integer> listItems;
            ArrayAdapter<Integer> adapter;


            listView = findViewById(R.id.pontszamok);
            listItems = new ArrayList<Integer>();
            listItems.addAll(MainActivity.PlayersPontsz);
            adapter = new ArrayAdapter<Integer>(this, R.layout.pontszamok, listItems);
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

        }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            this.finish();
            System.gc();


        }
        else if(keyCode == KeyEvent.KEYCODE_HOME)
        {
            System.exit(1);
        }

        if(KeyEvent.KEYCODE_POWER == event.getKeyCode()){
            System.exit(1);
        }
        return super.onKeyDown(keyCode, event);
    }


}
