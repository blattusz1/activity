package activity.blattusz.activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;

import android.os.Vibrator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import java.util.Collections;
import java.util.Random;

import static activity.blattusz.activity.MainActivity.WinnerPoint;

import static activity.blattusz.activity.MainActivity.jutalompont;
import static activity.blattusz.activity.MainActivity.roundnmb;

public class Game extends Activity {

    Button ora;
    private AudioManager audio;
    MediaPlayer startsound;
    MediaPlayer halfsound;
    MediaPlayer endsound;
    Vibrator v;
    TextView szo;
    TextView idotext;
    TextView type;

    int szamlalo = 1;

    boolean stop = false;
    Button nextbtn;
    Button rajz;
    Button addpontbutton;


    private InterstitialAd mInterstitialAd;

    private static final float BYTES_PER_PX = 4.0f;
    ImageView image;
    int kep;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game);

        ora = (Button)findViewById(R.id.time);
        ora.setVisibility(View.INVISIBLE);
        ora.setTextColor(Color.WHITE);

        audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
       // kijon = (TextView) findViewById(R.id.info);

        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        startsound = MediaPlayer.create(getApplicationContext(), R.raw.start);
        halfsound = MediaPlayer.create(getApplicationContext(), R.raw.half);
        endsound = MediaPlayer.create(getApplicationContext(), R.raw.end);
        type = (TextView) findViewById(R.id.type);
        type.setText("A következő páros");
        idotext = findViewById(R.id.idotext);

        image = (ImageView) findViewById(R.id.image_view);


        szo = (TextView) findViewById(R.id.szo);
        szo.setText(MainActivity.Players.get(MainActivity.playerindex) + "");
        MainActivity.playerindex++;
        ora.setVisibility(View.INVISIBLE);

        rajz = (Button) findViewById(R.id.rajztabla);
        rajz.setVisibility(View.INVISIBLE);

        Szavak.Rajz();
        Szavak.Mutogatas();
        Szavak.Beszed();
        Collections.shuffle(MainActivity.Rajz);
        Collections.shuffle(MainActivity.Mutogatas);
        Collections.shuffle(MainActivity.Beszed);




        MobileAds.initialize(this,
                "ca-app-pub-2285811773346460~2705615230");
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-2285811773346460/8031283476");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

        });



        rajz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(Game.this, Rajztabla.class);
                startActivity(i);


            }

        });



        addpontbutton = (Button) findViewById(R.id.pontadd);
        addpontbutton.setVisibility(View.INVISIBLE);

        addpontbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rajz.setVisibility(View.INVISIBLE);
                Intent i = new Intent(Game.this, AddPont.class);
                startActivity(i);
                addpontbutton.setVisibility(View.INVISIBLE);
                stop=true;
                idotext.setText("");
            }

        });

        Button timebtn;
        timebtn = (Button) findViewById(R.id.time);
        timebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Ora();

            }

        });


        nextbtn = (Button) findViewById(R.id.next);
        nextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                Feladat();
                idotext.setText("");
                for (Integer playerpoint : MainActivity.PlayersPontsz)
                {
                    if(WinnerPoint <= playerpoint && MainActivity.playerindex == 1)
                    {

                        Intent i = new Intent(Game.this, Eredmeny.class);
                        startActivity(i);
                        End();
                    }
                }




            }

        });


    }




    private void Feladat()
    {
        rajz.setVisibility(View.INVISIBLE);
        type.setText("");

        image.setVisibility(View.INVISIBLE);



        szamlalo++;





        if(roundnmb == MainActivity.Players.size())
        {
            roundnmb = 0;


        }
        roundnmb++;





        addpontbutton.setVisibility(View.INVISIBLE);




        if(szamlalo % 2 == 0 && szamlalo != 0)
        {

            image.setVisibility(View.VISIBLE);
           // Toast.makeText(getApplicationContext(), "Index:" + MainActivity.playerindex, Toast.LENGTH_LONG).show();




            long seed = System.nanoTime();
            Random rand = new Random(seed);
            int n = rand.nextInt(3);

            addpontbutton.setVisibility(View.VISIBLE);

            if(n == 0)
            {
                rajz.setVisibility(View.VISIBLE);

                szo.setText(MainActivity.Rajz.firstElement());
                MainActivity.Rajz.removeElement(MainActivity.Rajz.firstElement());
                CreatePontsz();
                kep = R.drawable.rajzicon;
                loadImage();
            }
            if(n==1)
            {

                szo.setText(MainActivity.Mutogatas.firstElement());
                MainActivity.Mutogatas.removeElement(MainActivity.Mutogatas.firstElement());
                CreatePontsz();
                kep = R.drawable.mutogatasicon;
                loadImage();
            }
            if(n==2)
            {

                szo.setText(MainActivity.Beszed.firstElement());
                MainActivity.Beszed.removeElement(MainActivity.Beszed.firstElement());
                CreatePontsz();
                kep = R.drawable.beszedicon;
                loadImage();
            }

            ora.setVisibility(View.VISIBLE);
        }
        else
        {
            type.setText("A következő páros");
            szo.setText(MainActivity.Players.get(MainActivity.playerindex) + "");


            if(MainActivity.playerindex == MainActivity.Players.size()-1)
            {
                MainActivity.playerindex = 0;
            }
            else
                MainActivity.playerindex++;

            ora.setVisibility(View.INVISIBLE);

        }


    }

    private void CreatePontsz()
    {
        type.setText("");
        if(szo.getText().toString().contains("2p"))
        {
            MainActivity.jutalompont = 2;

        }
        if(szo.getText().toString().contains("3p"))
        {
            MainActivity.jutalompont = 3;

        }
        if(szo.getText().toString().contains("4p"))
        {
            MainActivity.jutalompont = 4;

        }
        if(szo.getText().toString().contains("5p"))
        {
            MainActivity.jutalompont = 5;

        }
        if(szo.getText().toString().contains("6p"))
        {
            MainActivity.jutalompont = 6;
            type.setText("Nyílt kör!");
        }
    }

    public void Ora() {
        v.vibrate(300);
        ora.setVisibility(View.INVISIBLE);

        nextbtn.setEnabled(false);

        if(startsound.isPlaying())
            startsound.stop();
        startsound.start();


        if(szamlalo % 5 == 0)
        {
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            } else {
                Log.d("TAG", "The interstitial wasn't loaded yet.");
            }

        }



        new CountDownTimer(MainActivity.ido, 1000)
        {
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            boolean isScreenOn = pm.isScreenOn();
            public void onTick(long millisUntilFinished) {

                idotext.setTextColor(Color.BLACK);
                idotext.setText(millisUntilFinished/1000+"");


                isScreenOn = pm.isScreenOn();

                if(isScreenOn == false)
                    this.cancel();

                if(stop == true)
                {
                    this.cancel();
                    idotext.setText("Vége");
                    stop=false;
                    nextbtn.setEnabled(true);

                }
                if((millisUntilFinished/1000 > 60 && millisUntilFinished/1000 < 65 )|| (millisUntilFinished/1000 > 0 && millisUntilFinished/1000 < 6 ))
                {
                    if(startsound.isPlaying())
                        startsound.stop();
                    startsound.start();
                }




                if(millisUntilFinished/1000 % 60 == 0 && szamlalo > 1 && millisUntilFinished/1000 > 1)
                {
                    if(halfsound.isPlaying())
                        halfsound.stop();
                    halfsound.start();
                    jutalompont = 6;
                    type.setText("RABLÁS!!! 6p");
                    v.vibrate(500);
                }
            }



            public void onFinish() {


                nextbtn.setEnabled(true);
                if(szamlalo > 1 )
                {
                    if(endsound.isPlaying())
                        endsound.stop();
                    endsound.start();

                    v.vibrate(1000);
                }

                idotext.setTextColor(Color.BLACK);
                idotext.setText("");


                ora.setBackgroundResource(R.drawable.time);
                ora.setVisibility(View.INVISIBLE);
                rajz.setVisibility(View.INVISIBLE);

            }
        }.start();
    }



    public void End()
{

    this.finish();
    System.gc();

}


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false;

			
			
        }
        else if(keyCode == KeyEvent.KEYCODE_HOME)
        {
            System.exit(1);
        }

        if(KeyEvent.KEYCODE_POWER == event.getKeyCode()){
            System.exit(1);
        }
        return super.onKeyDown(keyCode, event);
    }


    private void loadImage() {
        if(readBitmapInfo() > ConvertImg.megabytesFree()) {
            subSampleImage(32);
        } else {
            image.setImageResource(kep);
        }
    }



    private float readBitmapInfo() {
        final Resources res = this.getResources();
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, kep, options);
        final float imageHeight = options.outHeight;
        final float imageWidth = options.outWidth;
        final String imageMimeType = options.outMimeType;
        return imageWidth * imageHeight * BYTES_PER_PX / ConvertImg.BYTES_IN_MB;
    }

    private void subSampleImage(int powerOf2) {
        if(powerOf2 < 1 || powerOf2 > 32) {
            return;
        }

        final Resources res = this.getResources();
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.inSampleSize = powerOf2;
        final Bitmap bmp = BitmapFactory.decodeResource(res, kep, options);

        image.setImageBitmap(bmp);
    }

    public void End2()
    {
        this.finish();
    }




}
