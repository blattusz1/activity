package activity.blattusz.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.content.Intent;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;


import androidx.appcompat.view.ContextThemeWrapper;

import java.util.ArrayList;


public class AddPlayer extends Activity implements SeekBar.OnSeekBarChangeListener{


    private static SeekBar SecBar;
    private TextView SecText;



    @Override
    protected void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.addplayer);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);





        SecText = (TextView)findViewById(R.id.SecProgressId);
        SecText.setText("Győzelemhez szükséges pontszám: 60 pont (Normál játék)");


        SecBar = (SeekBar)findViewById(R.id.SecBarId); // make seekbar object
        SecBar.setOnSeekBarChangeListener(this);
        SecBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // TODO Auto-generated method stub
                SecText = (TextView)findViewById(R.id.SecProgressId);
                progress = ((int)Math.round(progress/30))*30;
                seekBar.setProgress(progress);

                if(progress == 0)
                    progress = progress+30;

                SecText.setText("Győzelemhez szükséges pontszám: " + progress);


                MainActivity.WinnerPoint = progress;

            }
        });







        if(MainActivity.Players.size() > 0)
        {

            ListView listView;
            ArrayList<String> listItems;
            ArrayAdapter<String> adapter;



            listView = (ListView) findViewById(R.id.players);
            listItems = new ArrayList<String>();
            listItems.addAll(MainActivity.Players);
            adapter = new ArrayAdapter<String>(this, R.layout.playerlist, listItems);
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
/*
        MainActivity.Players.addElement("1 és Jani");
        MainActivity.Players.addElement("2 és Fanni");
        MainActivity.Players.addElement("3 és Pityu");
        MainActivity.Players.addElement("4 és Pityu");

        MainActivity.PlayersPontsz.addElement(0);
        MainActivity.PlayersPontsz.addElement(0);
        MainActivity.PlayersPontsz.addElement(0);
        MainActivity.PlayersPontsz.addElement(0);*/



        RefreshList();

        final EditText player1 = (EditText) findViewById(R.id.player1);
        final EditText player2 = (EditText) findViewById(R.id.player2);
        player1.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {

                    String s = player1.getText().toString();
                    if(!s.equals(""))
                    {
                        player2.setEnabled(true);
                    }
                    else
                    {
                        TextView tvSSID1 = (TextView) findViewById(R.id.rossz);
                        tvSSID1.setText("Érvénytelen Név");
                    }


                }
                return false;
            }
        });
        player2.setEnabled(false);
        player2.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {

                    String s = player2.getText().toString();
                    if(!s.equals(""))
                    {

                        TextView tvSSID0 = (TextView) findViewById(R.id.rossz);
                        tvSSID0.setText("");
                        if(MainActivity.Players.contains(player1.getText().toString() + " és " + s))
                        {
                            TextView tvSSID1 = (TextView) findViewById(R.id.rossz);
                            tvSSID1.setText("Érvénytelen Név");
                            return false;
                        }
                        else
                        {
                            MainActivity.Players.addElement(player1.getText().toString() +" és "+s);
                            MainActivity.PlayersPontsz.addElement(0);
                            TextView tvSSID1 = (TextView) findViewById(R.id.player1);
                            tvSSID1.setText("");
                            TextView tvSSID2 = (TextView) findViewById(R.id.player2);
                            tvSSID2.setText("");
                            player2.setEnabled(false);

                            RefreshList();
                        }

                    }
                    else
                    {
                        TextView tvSSID1 = (TextView) findViewById(R.id.rossz);
                        tvSSID1.setText("Érvénytelen Név");
                    }


                }
                return false;
            }
        });


        Button bn1;
        bn1 = (Button)findViewById(R.id.start);
        bn1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if(MainActivity.Players.size() < 2)
                {
                    TextView tvSSID1 = (TextView) findViewById(R.id.rossz);
                    tvSSID1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                    tvSSID1.setText("Minimum 2 csapat kell!");
                }
                else
                {


                    @SuppressLint("RestrictedApi") AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(AddPlayer.this, R.style.AlertDialogCustom));
                    builder.setMessage("Alkossatok párokat, majd írjátok be őket az applikációba! (Minimum 2 csapat kell)\n\n" +
                            "Állítsátok be a győzelemhez szükséges pontszámot, mely a játék hosszát is meghatározza!\n\n" +
                            "A játék indítása után, a soron következő csapat neve fel lesz tüntetve.\n\n" +
                            "Ti dönthetitek el, hogy melyikőtök kezdi a csapaton belül a feladat teljesítését.\n\n" +
                            "Ezután kattintsatok a képernyő közepére, a szót csak a feladatot teljesítő személy láthatja.\n\n" +
                            "A hagyományos Activity-hez hasonlóan a feladatokat lerajzolni, elmagyarázni vagy elmutogatni kell\n" +
                            "a csapattársadnak. Amennyiben ezt nem sikerül teljesítened 1 percen belül, a feladat nyílt körré\n" +
                            "válik, azt bárki kitalálhatja!\n\n" +
                            "Az applikáció tartalmaz nyílt körös szavakat is.\n\n" +
                            "Jó szórakozást!");
                    builder.setCancelable(false);
                    builder.setTitle("\nJátékszabály!\n");
                    builder.setPositiveButton("Elolvastam",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();


                                    Intent i = new Intent(AddPlayer.this, Game.class);
                                    startActivity(i);
                                    End2();


                                }
                            });

                    AlertDialog dialog = builder.create();
                    dialog.show();




                }


            }

        });
    }

    public void End2()
    {
        this.finish();
    }
    public void RefreshList()
    {
        if(MainActivity.Players.size() > 0)
        {
            ListView listView;
            ArrayList<String> listItems;
            ArrayAdapter<String> adapter;


            listView = (ListView) findViewById(R.id.players);
            listItems = new ArrayList<String>();
            listItems.addAll(MainActivity.Players);
            adapter = new ArrayAdapter<String>(this, R.layout.playerlist, listItems);
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

        }
    }




    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false;
        }
        else if(keyCode == KeyEvent.KEYCODE_HOME)
        {
            System.exit(1);
        }

        else if(keyCode == KeyEvent.KEYCODE_VOLUME_UP)
        {
            return true;
        }

        else if(keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
        {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }




    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (seekBar == SecBar)
        {
            SecText.setText("Győzelemhez szükséges pontszám: " + progress);
        }
        MainActivity.WinnerPoint = progress;



    }


    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // TODO Auto-generated method stub

    }
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        // TODO Auto-generated method stub

    }





}