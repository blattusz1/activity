package activity.blattusz.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

public class Rajztabla extends Activity {

    private CanvasView customCanvas;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rajztabla);




        customCanvas = (CanvasView) findViewById(R.id.signature_canvas);


        Button exit;
        exit = (Button) findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                End();

            }

        });


    }


    private void End()
    {
        this.finish();
    }


    public void clearCanvas(View v) {

        customCanvas.clearCanvas();

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return false;



        }
        else if(keyCode == KeyEvent.KEYCODE_HOME)
        {
            System.exit(1);
        }

        if(KeyEvent.KEYCODE_POWER == event.getKeyCode()){
            System.exit(1);
        }
        return super.onKeyDown(keyCode, event);
    }

}

