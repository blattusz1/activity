package activity.blattusz.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class Eredmeny extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eredmeny);

        ListView listView;
        ArrayList<String> listItems;
        ArrayAdapter<String> adapter;

        ListView listView2;
        ArrayList<Integer> listItems2;
        ArrayAdapter<Integer> adapter2;


        listView = (ListView) findViewById(R.id.players);
        listItems = new ArrayList<String>();
        listItems.addAll(MainActivity.Players);
        adapter = new ArrayAdapter<String>(this, R.layout.playerlist, listItems);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();






        listView2 = (ListView) findViewById(R.id.pontszamok);
        listItems2 = new ArrayList<>();
        listItems2.addAll(MainActivity.PlayersPontsz);
        adapter2 = new ArrayAdapter<Integer>(getApplicationContext(), R.layout.pontszamok, listItems2);
        listView2.setAdapter(adapter2);
        adapter2.notifyDataSetChanged();


        RefreshList();



        Button backbtn = (Button) findViewById(R.id.back);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                End();
            }
        });

    }

    public void End()
    {
        MainActivity.playerindex = 0;
        MainActivity.PlayersPontsz.clear();
        MainActivity.Players.clear();
        MainActivity.jutalompont=0;
        MainActivity.roundnmb=0;
        MainActivity.Rajz.clear();
        MainActivity.Beszed.clear();
        MainActivity.Mutogatas.clear();
        System.gc();
        this.finish();
    }

    public void RefreshList()
    {

            ListView listView;
            ArrayList<Integer> listItems;
            ArrayAdapter<Integer> adapter;


            listView = findViewById(R.id.pontszamok);
            listItems = new ArrayList<Integer>();
            listItems.addAll(MainActivity.PlayersPontsz);
            adapter = new ArrayAdapter<Integer>(this, R.layout.pontszamok, listItems);
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

        }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

           return false;


        }
        else if(keyCode == KeyEvent.KEYCODE_HOME)
        {
            System.exit(1);
        }

        if(KeyEvent.KEYCODE_POWER == event.getKeyCode()){
            System.exit(1);
        }
        return super.onKeyDown(keyCode, event);
    }


}
